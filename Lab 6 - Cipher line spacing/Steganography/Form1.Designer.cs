﻿namespace Steganography
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.addressLabel = new System.Windows.Forms.Label();
            this.openButton = new System.Windows.Forms.Button();
            this.txtEncryptLabel = new System.Windows.Forms.Label();
            this.txtEncrypt = new System.Windows.Forms.TextBox();
            this.encryptButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // addressLabel
            // 
            this.addressLabel.AutoSize = true;
            this.addressLabel.Location = new System.Drawing.Point(217, 23);
            this.addressLabel.Name = "addressLabel";
            this.addressLabel.Size = new System.Drawing.Size(0, 13);
            this.addressLabel.TabIndex = 0;
            // 
            // openButton
            // 
            this.openButton.BackColor = System.Drawing.Color.Bisque;
            this.openButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.openButton.Location = new System.Drawing.Point(17, 18);
            this.openButton.Name = "openButton";
            this.openButton.Size = new System.Drawing.Size(169, 23);
            this.openButton.TabIndex = 1;
            this.openButton.Text = "Открыть документ";
            this.openButton.UseVisualStyleBackColor = false;
            this.openButton.Click += new System.EventHandler(this.openButton_Click);
            // 
            // txtEncryptLabel
            // 
            this.txtEncryptLabel.AutoSize = true;
            this.txtEncryptLabel.Location = new System.Drawing.Point(14, 46);
            this.txtEncryptLabel.Name = "txtEncryptLabel";
            this.txtEncryptLabel.Size = new System.Drawing.Size(162, 13);
            this.txtEncryptLabel.TabIndex = 0;
            this.txtEncryptLabel.Text = "Введите символы для шифра: \r\n";
            // 
            // txtEncrypt
            // 
            this.txtEncrypt.Location = new System.Drawing.Point(204, 43);
            this.txtEncrypt.Name = "txtEncrypt";
            this.txtEncrypt.Size = new System.Drawing.Size(260, 20);
            this.txtEncrypt.TabIndex = 2;
            // 
            // encryptButton
            // 
            this.encryptButton.BackColor = System.Drawing.Color.Bisque;
            this.encryptButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.encryptButton.Location = new System.Drawing.Point(323, 69);
            this.encryptButton.Name = "encryptButton";
            this.encryptButton.Size = new System.Drawing.Size(141, 23);
            this.encryptButton.TabIndex = 3;
            this.encryptButton.Text = "Зашифровать";
            this.encryptButton.UseVisualStyleBackColor = false;
            this.encryptButton.Click += new System.EventHandler(this.encryptButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightGray;
            this.ClientSize = new System.Drawing.Size(476, 99);
            this.Controls.Add(this.encryptButton);
            this.Controls.Add(this.txtEncrypt);
            this.Controls.Add(this.addressLabel);
            this.Controls.Add(this.txtEncryptLabel);
            this.Controls.Add(this.openButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "Form1";
            this.Text = "Метод межсимвольного интервала";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button encryptButton;
        private System.Windows.Forms.TextBox txtEncrypt;
        private System.Windows.Forms.Label txtEncryptLabel;
        private System.Windows.Forms.Button openButton;
        private System.Windows.Forms.Label addressLabel;
    }
}

