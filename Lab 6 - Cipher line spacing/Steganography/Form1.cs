﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using Word = Microsoft.Office.Interop.Word;

namespace Steganography
{
    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();
        }

        private void openButton_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                addressLabel.Text = openFileDialog1.FileName;
            }
        }

        private void encryptButton_Click(object sender, EventArgs e)
        {
            try
            {
                object positionLetter = 0;
                object w = 1;

                Word.Application app = new Word.Application();
                app.Visible = true;
                var doc = app.Documents.Open(openFileDialog1.FileName);

                string input = txtEncrypt.Text;

                int i = 0;
                while (true)
                {
                    var range = doc.Range(ref positionLetter, ref w);
                    char c = input[i];
                    if (c == Convert.ToChar(range.Text))
                    {
                        range.Select();
                        range.Font.Spacing = 2;
                        positionLetter = (int)positionLetter + 1;
                        w = (int)w + 1;
                        i++;
                    }
                    else
                    {
                        positionLetter = (int)positionLetter + 1;
                        w = (int)w + 1;
                    }

                    if (i == input.Length)
                        break;
                }


                doc.Save();
                //doc.Close();
                //app.Quit();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        
    }
}
